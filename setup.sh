#!/usr/bin/env bash


# Unsure if these are needed please uncomment if needed
#sudo apt-get update
#
#sudo apt-get install python-pip
#
#sudo apt-get install virtualenv

venv=env

# create virtualenv if it doesn't exist, then activate
if [ ! -d $venv ]; then
    virtualenv
    virtualenv -p python3 $venv
fi
. $venv/bin/activate


pip install -U pip setuptools
pip install -U pip-tools
pip-sync

cd api_demo

export DJANGO_SETTINGS_MODULE=config.settings

./manage.py migrate

# Add the django data
./manage.py loaddata start_data.json

#Add the data from nasa
./manage.py add_new

./manage.py runserver

