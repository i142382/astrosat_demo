from django.core.management.base import BaseCommand
from nasa.views import add_new_facilities


class Command(BaseCommand):
    help = "Add new facilties "
    requires_system_checks = False

    def handle(self, *args, **options):
        add_new_facilities()

