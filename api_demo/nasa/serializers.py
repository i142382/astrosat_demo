import dateutil.parser
from rest_framework import serializers
from rest_framework.fields import DictField

from .models import Facility


class FacilitySerializer(serializers.ModelSerializer):

    location = DictField()

    class Meta:
        model = Facility
        fields = ('center', 'center_search_status', 'facility', 'occupied', 'status', 'url_link',
                  'last_update', 'zipcode', 'phone', 'record_date', 'city', 'state', 'country',
                  'contact', 'location')

    def create(self, validated_data):
        f = validated_data

        # Create useable dates
        if 'last_update' in validated_data:
            f['date_last_update'] = dateutil.parser.parse(validated_data['last_update'])
        if 'record_date' in validated_data:
                f['date_record'] = dateutil.parser.parse(validated_data['record_date'])
        if 'occupied' in validated_data:
                f['date_occupied'] = dateutil.parser.parse(validated_data['occupied'])

        # Unable to serialize location directly, populate location_point field
        f['location_point'] = f['location']
        return Facility.objects.create(**validated_data)


class FacilityListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Facility
        fields = ('center', 'center_search_status', 'facility', 'date_occupied', 'status', 'url_link',
                  'date_last_update', 'zipcode', 'phone', 'date_record', 'city', 'state', 'country',
                  'contact', 'location_point')




