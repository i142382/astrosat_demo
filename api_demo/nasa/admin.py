from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from .models import Facility


@admin.register(Facility)
class FacilityAdmin(LeafletGeoAdmin):
    fields = ('center', 'center_search_status', 'facility', 'status', 'url_link', 'zipcode',
              'phone', 'city', 'state', 'country', 'contact', 'date_last_update', 'date_record', 'date_occupied',
              'location_point')

    list_display = ('facility', 'center', 'status', 'url_link', 'zipcode',
                    'phone', 'city', 'state', 'country', 'contact')

    search_fields = ['center', 'facility', 'city', 'zipcode', 'contact']

    list_filter = ('center', 'city', 'state')

    # Read only Field to identify the Facility
    readonly_fields = ('facility',)



