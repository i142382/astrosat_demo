from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^facility_list/$', views.FacilityList.as_view()),
]