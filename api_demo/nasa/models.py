from django.db import models
from djgeojson.fields import PointField


class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Facility(TimeStampedModel):
    center = models.CharField(max_length=100, db_index=True)
    center_search_status = models.CharField(max_length=10, db_index=True)
    facility = models.CharField(max_length=200, unique=True)
    occupied = models.CharField(max_length=100, null=True, blank=True)
    status = models.CharField(max_length=100, null=True, blank=True)
    url_link = models.CharField(max_length=150, null=True, blank=True)
    last_update = models.CharField(max_length=100, null=True, blank=True)
    zipcode = models.CharField(max_length=50, db_index=True)
    phone = models.CharField(max_length=50)
    record_date = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, db_index=True)
    state = models.CharField(max_length=2, db_index=True)
    country = models.CharField(max_length=10)
    contact = models.CharField(max_length=150)
    location = models.CharField(max_length=100, blank=True, null=True)
    # Amended fields not from api
    date_last_update = models.DateField(null=True, blank=True)
    date_record = models.DateField(null=True, blank=True)
    date_occupied = models.DateField(null=True, blank=True)
    location_point = PointField(null=True)

    def __str__(self):
        return self.facility

    class Meta:
        verbose_name_plural = "Facilities"
