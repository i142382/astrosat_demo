import requests
from background_task import background
from django.contrib.sites import management
from rest_framework import generics
from rest_framework.renderers import JSONRenderer
from rest_framework_swagger.views import get_swagger_view

from .models import Facility
from .serializers import FacilitySerializer, FacilityListSerializer


def add_new_facilities():
    r = requests.get("https://data.nasa.gov/resource/9g7e-7hzz.json")
    json = r.json()
    for j in json:
        serializer = FacilitySerializer(data=j)
        if serializer.is_valid():
            # We dont want to add any Facilities we already have, use the name in place of anything better
            # Remove trailing whitespace to match query
            if not Facility.objects.filter(facility=j['facility'].rstrip()).exists():
                serializer.save()


class FacilityList(generics.ListAPIView):

    renderer_classes = (JSONRenderer,)

    queryset = Facility.objects.filter(status='Active')
    serializer_class = FacilityListSerializer


def schedule_call():
    management.call_command('add_new')


# Show a front page with all API
schema_view = get_swagger_view(title='Demo API')
