from django.contrib import admin
from django.conf.urls import url, include
from nasa import views


urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'nasa/', include('nasa.urls')),
    url(r'^$', views.schema_view)
]